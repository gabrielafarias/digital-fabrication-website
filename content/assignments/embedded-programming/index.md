+++
title = "Embedded Programming"
+++

## Soldering

I soldered my board and it was easy and cool to do tiny welding.

I turned the machine on with two fingers, I wet the sponge, I turned on the exhaust fan.

![turning on solder machine](turning-on-solder.jpg)

I then tested the solder to make sure the fan was working.

![solder station](solder-station.jpg)

We worked together to make sure we positioned the board correctly to solder it! I soldered each little guy by heating the solder and making a bit of a pool, then using the soldering gun to smooth the little peaks it made. Fun for me to do!

![gab soldering](gab-solder.jpg)

![soldered board](soldered.jpg)

Once I put my XIAO board on my breadboard, 2 of the pins came loose. I realize after some feedback from Not TopTechBoy Paul that I didnt add enough solder.

![broken xiao board]

I added some more solder and I think I could add more, but this is good enough:

![newly finished xiao board]

## Arduino with Paul

The notes that follow are related to watching a video with my [TopTechBoy](https://www.youtube.com/watch?v=fJWR7dBuc18)

- Download the software
- Connect the Arduino to the software via tools>port
- Paul explains that the pins are numbered and we can pull signals off the port or send signals to the port
- When Arduino software turns orange: that’s a happy thing
- Bumpy font: don’t use spaces, use capitals to identify next word
- Blue: also good news
- End almost everything with a ;

## Arduino with Kris

These are my step by step notes from Kris’s lecture:

- You use SEEED to program microcontrollers and computers
- This language can be used for assembly or bits and bytes or object code
- The language has important elements, ex VARIABLES
- Start with type of variable: here we use “int” for INTEGER
- Then number = __;
- _Char_ for character, write a letter which really corresponds with another number
- So we can just use integer more often in this situation
- Void function definition. Means will return nothing
- If you have a function, you might have something to return
- Functions you use to summarize or modularize your code
- 2 main functions
    - _Void setup:_ describes being run only once as Arduino sets up
    - In the brackets you put function parameters. In curly brackets you define the start and end of function body
    - _pinMode_ is Arduino specific function
    - On the Seeed board:
        - Pins D0-D10 can be defines as inputs and outputs on the SEEED board.
        - So in pinMode you specify number or input or output
- In _pinMode_, the LED is choosing which of these inputs you want to focus on
- In _void loop_, you specifiy led or pin number and a certain value
    - In XIAO boards, we have opposite!
    - High means it turns off. LOW means it turns on
    - Arduino pins [file](https://mycourses.aalto.fi/pluginfile.php/1945496/mod_resource/content/2/pins_arduino.h)
    - Best place to access what you can do within Arduino
    - For example: By using the PIN_LED_G input, I made the green light blink

![green light blink](green-light-blink.jpg)

{{<video src ="green-light.mp4">}}{{</video>}}

## BlinkRGB

So then we did a complicated example with Kris: this time we blink all the red green and blue LEDs.

A variable that defines the delay
The setup runs once at beginning of powering up the bord, then setting up the LED pins to output so switched on and off and drive some current through them

Then loop function, runs over and over forever until board runs out of power (disconnect it)
- And then calling a function: blink and passing in a parameter
- Red then green then blue

Then you can create your own functions to simplify the code a little bit
> “Functions are good” – wise words from Kris

So when you define the function, then obviously you need to specify the return type. Even though the function doesn’t return anything, like void, you need space then name and in the parenthesis you need to specify what parameters you want to use in the function.

So on this more complicated run, I was having trouble. In the end I realized I had made the word “pin” capitalized in _pinMode_ on one of my void setup commands. Once I corrected the capitalization, the script ran properly:

{{<video src ="blink-rgb.mp4">}}{{</video>}}

## NEOPIXELS

What is a neopixel you ask?
-	Adafruit.com is where you can get gorgina boards like beopixel rings or sticks
-	“addressable LEDs” means you can control them from a microcontroller
-	But here: it is only one neopixel.

First, I downloaded the Neopixel by Adafruit library. Then there are examples available.
But we worked from one of the XIAO board examples here:

![neopixel screenshot](Neopixel.jpg)

The code starts with the library I downloaded.
In this example, I need a button connected.

I define the power pin (powPIN)
D10 has two terminals. One needs to be connected to D10 and one connected to Ground (GND). So it will be LOW (flipped)

So specify the type (AdaFruit_NeoPixel), then specify name (pixels here but can be pix), then number of pixels, which is above translating to one, then the pin (neoPin), and then parameters of how color mapping is working on the board (RGB to GRB) and then certain frequency (NEO_khz800)

- powPIN gives power to the pixel
- OUTPUT gives power to the power pin in a way
- btnPIN, use INPUT_PULLUP resistor
- and then you also need to power it on using digitalWrite

Then the loop!
- Read the state of the button, (btnState)
- digitalRead will read btnPin D10
- so when press button, D10 and button will connect
- so we read the button and then we identify it as LOW so when pressed, some stuff happens as seen below
- “if” = what it says. So if something is true
- True means will always do this
- If it is false it will skip to the next section
- Check out :Comparison Operators”
- See [this reference](https://reference.arduino.cc/reference/en/language/structure/comparison-operators/equalto/)

0-255? Because for each channel on the neopixel LEDs, the value can be one byte, which is 256 possible values
-	A byte is 8 bits
-	8 1’s, 8 bits, in decimal form is 255
-	256 values including 0
-	If it’s 8 0s, and change one of the 0s to a 1, the decimal conversion changes.
    - See [here](https://www.rapidtables.com/convertnumber/binary-to-decimal.html?x=0000011)

Anyway! Using the code offered in the XIAO example board that Kris went over, putting the XIAO board on the breadboard, and connecting the wires to the ground, D10, and the other side of both cables on either side of the button’s connections in the horizontal line? IT WORKED

{{<video src ="rgb-laser.mp4">}}{{</video>}}

I did add my own void setup line “pixels.setBrightness (20);” to turn down the blinding light.

![brightness](brightness.jpg)

How do you communicate between the board and the computer?
-	We open a “serial.ino” file
-	Once again:
-	Setup function that says what board does when it starts up and the loop
-	Other word is UART
-	2 wires: one for sending and one for receiving data
    - TX and RX
    - There’s pins marked on the XIAO board

![xiao board](xiao-board.jpg)

The TX and RX are connected to the USB. There are 5volts regulated to 3.3 volts, because RP2040 operates on that… the powerlines RX and TX allow the computer to communicate to this board.

Serial is one of a few communication protocols. But it is the basic one.

Serial.begin is 19200: this specifies how fast it is sending and receiving data (by bits per second, in a second it can send 19200 bits, which is 19200/8= 2400 bytes per second)

Using the serial monitor, you can change the value of communication up to 2million. If you want to increase speed, you are able to.

_while loop:_
-	We want to check if Serial available returns us any value
-	Get the number of bytes available for reading the serial port
-	It runs the body of the while loop as long as the serial available is larger than 0
-	If it returns anything more than 0, than it will evaluate as through as long as it is through it will continuously run
-	What does serial.read mean?? **

-	Print: will print whats there without change
-	Println: will print what you give it as a parameter and add a new line at the end of it

So as I ran the code and uploaded it, as I send a message in the Serial Monitor, I see the red tiny led corresponds with the number of blinks. So 5 letters: 5 blinks

{{<video src ="five-blinks.mp4">}}{{</video>}}

Added the DEC to the Serial.println code. Now numbers translate to big numbers. Why?!
-	The ASCII table! A table which maps decimal values to certain values on your keyboard or certain symbols to certain decimal numbers… See more here: https://www.asciitable.com/
-	So for example: 1 is 49.
THEN we tried in Binary
-	The binary goes up as the number increases 
-	So yeah… these are the basics from Kris’s video.

## Additional Examples

_Button example_
I opened this example in the XIAO examples and this allowed me to press the button on my breadboard which made the small led change color

Neopixel example for RGB
-	I simply added the pixels.setBrightness (20) again so my pupils could avoid searing pain

{{<video src ="long-hold-blue.mp4">}}{{</video>}}

I was curious was the pixel colour value means, so I googled it and found a bit of information on hue, saturation and [value](https://learn.adafruit.com/adafruit-neopixel-uberguide/arduino-library-use)

![pixel-colour](pixel-colour.jpg)

## BONUS LESSON W MY ROOMMATE PAUL 

Paul is like a major arduino/transistor/binary math nerd, not to be confused with Paul McWhorter, my [TopTechBoy](https://toptechboy.com/) not to be confused with and he helped me so much. I feel like I followed the lesson by Kris, but I didn't really understand how it worked. Paul totally enlightened me!!!

-	D= digital
-	A=analog
    - The difference between them is the digital is the representation of the analog world represented through 1 and 0.
    - Distinct and finite state that is an approximation using 1s and 0s
-	Why 1s and 0s?
    - The transistor! Is why we have binary
    - It is a 3 pin device that is a switch: the most basic digital switch
    - Silicon (semi-conductor that can change properties of to conduct little to a lot to not at al)
        - How we can build switches, for example
    - We realized early in 1920s by controlling the current thru these devices we can represent off-state as 0 and on-state as a 1
    - 2 transistors stores 1 bit
    - Millions of transistors in the freaking seeed board!
    - Billions in the solid state memory of my mac!
- So anyway – we store digital information on a transistor
- And then we made the math that can be stored as a 1 and a 0
     - High and low in technical terms

Why we use “input_pullup”:

- The pin is floating when it is undefined – so when the button isn’t pushed in, it could be ANYTHING. It doesn’t have a state it exists in. You want it to have a defined value.
- It would be great when we push the button and release it that it would always be in ground and when we push it would be 5volts, but you can have something grounded and connected to voltage at same time (think like both sides of a car battery)
- How do we connect without making them shortcircuit? Its ok if something is between them

- Potentiometer: ANALOG VALUE
- Analog to digital converter: ADC

This was all really helpful in grounding in the concepts.

![paul notes on analog vs digital](paul-notes-analog-digital.jpg)
![notes on xiao vs basic](notes-xiao-basic.jpg)
![paul explains binary](paul-explains-binary.jpg)

As we were talking and Paul was explaining digital vs analog, he was like, we should try it! And so we did using a potentiometer to see how the values change from 0s and 1s to 0-1024 in analog. As we moved the POTmeter, we saw the values change on the serial monitor:

{{<video src ="serial-monitor.mp4">}}{{</video>}}

Then with the Neopixel!

{{<video src ="pot-meter.mp4">}}{{</video>}}

This was the code for the Neopixel analog sketch:

![neopixel analog](neopixel-analog.jpg)

Pretty fun week! Wish Paul (not TopTechBoy) was here the past 5 weeks!

### Additional Accomplishments

In addition, this week I also accomplished the following:

- I did a solvespace tutorial successfully and updated the solvespace entry on my CAD page
- I did some updates to my website including margins and centering images

I want to:

- learn how to make things look like code vs screenshots like how Burak does
- make photos go beside each other

