+++
title = "Computer Controlled Cutting"
+++

## Laser Cutting Introduction

_Safety_

When trained on the machine, Kris explained first steps of safety to using the laser cutter.

1. Turn on the Air Assist to keep combustible gases away from the cutting surface and to flammable material flareups.
2. In case of burning or fire, press the emergency stop blanket and use the fire blanket or extinguisher.
3. The laser cutter can never be left unattended. There is a key card beside the machine to activate it which asks if you’re present ever 5 minutes. Be sure to press the “I’m here” button.

![safety first](safety-first.jpeg)

## The laser cut design process - Fusion360

_1. Create a design in CAD_

- For my first project, I created a finger join box using a tutorial. That process allowed me to properly use parameters and now I see how necessary the parameters are to accurately adjust for kerf.

- The tutorial for the box felt really messy and like maybe there was an easier way, especially when I was pulling the DXF files into Illustrator and there were so many unnecessary lines.

![bad illustrator dxf](bad-illustrator-dxf.jpg)

_2. Export the 2D design from Fusion to Illustrator_

- This is best done as a dxf file. I got a DXF plugin for fusion, so now I can pull the bodies a part in my Fusion file, and export them as DXF without unwanted lines, like construction lines.

![how to save dfx](save-dfx.jpg)

- If this doesn’t work, you may need to use the position tool to make sure the dxf plugin understands which plane you are trying to save the dxf of. The location of the body, if pulled away from the full model, may need to be identified its rightful home. See here:

![dfx position button](dxf-position-piece.jpg)

- You can save the dxf files to your OneDrive to get them to Illustrator on one of the school computers.

_3. Join lines, use vector lines, and choose stroke width of lines_

- You want the lines to be 0.01mm
- If the dfx still brought in unwanted lines, you might need to ungroup in order to clean it up properly.
- You click  “print” when you’re ready, choose “custom” and the “Epilog Engraver”

How to change lines:

![image trace](image-trace.jpeg)

Settings for sending to Epilog:

![custom after click print](custom-when-click-print.jpeg)

_4. Use Epilog software to optimize and prepare the cut_

- From here, you can select the material
- You can use the camera system in the laser cutter to decide where you want the design to be cut on that material
- If the camera doesn’t work, restart the machine
- Here you can also adjust speed, power and frequency

## First lasercutting session

_Square/Kerf Test_

Sara, Pegah and I did our own square test to measure kerf before we began cutting our own projects. After following the safety protocol, we were ready to get going! We put the cardboard in the machine, and used two hands.

![opening laser cutter](opening-the-machine.jpeg)

We did not create a fusion file for this, we went straight to Illustrator and made 3 squares, each 30x30mm and set the line to 0.01mm. We also drew an A on one of them to test the engraver.

![first squares](squares.jpeg)

The first time we cut was way out of focus, the line was quite thick and it was not possible to easily remove the squares from the cardboard. This was mainly because the material was slightly curved, but we learned we could use tape to secure it to the table.

![bad squares](first-squares-bad.jpeg)

We calibrated the distance between the laser and the material to get the ideal focal point. Pegah joined us and took a turn, but forgot to set the material, so the next batch the settings were too strong and cut through the engrave part. But then we did it again and it was PERFECT.

![sample squares](sample-squares.jpeg)

Then we did our kerf test. Since our squares were 30mm x 30mm square, we cut the three of those without adjustments, together they were 89.4 mm. So we made the squares 0.2mm bigger, and then we got the 90.0mm we were looking for.

![89.4 square](89-square-test.jpeg)
![post kerf edjust](post-kerf-adjustment.jpeg)

_Kerf with Group A_
In our training, we covered kerf. I was a part of Group 1 for the laser cutter introduction. For notes on kerf, you can access them [here](https://aaltofablab.gitlab.io/laser-cutting-2023-group-a/kerf.html)

## The laser cutting process – My time has come

I created a simple box using a tutorial. The link to the tutorial is [here](https://www.youtube.com/watch?v=ZrcqauNvt0M&t=1s)

I first watched a video on how to set up parameters. You can see [here](https://www.youtube.com/watch?v=H6W-Og4YyZ8)

The parameters completely changed my experience of Fusion. I see how useful these are for future designing, to go back and adjust elements of my design that may change.

![setting parameters](setting-parameters.jpg)
 
For example, when I set my parameters for height and width, and then went to my first sketch, I was able to just type in “Height” and “width” to get the distance I wanted. If later the height were to change, I can simply change the parameter. When I extrude the original rectangle, I can click “ply” to say how far to extrude. Cool!

![height sketch](height-sketch.jpg)
![ply tutorial](ply-tutorial.jpg)

I then drew small rectangles at the top edge of the rectangle and construction lines in between them from the midpoint (this is KEY). You want to make sure everything is horizontal and vertical or else you’re not going to complete the “Equal” function correctly. 

![messed up equal](messed-up-equal.jpg)

I didn’t realize this so I called my friend Martin who offered morale support until I got this right! He didn't know Fusion 360 but was happy to make sure I avoided nervous breakdown :)

![martin zoomin](martin-help.jpg)

Then I was able to mirror those teeth I made across the image, and do the same to the othee side, and also mirror on this sketch.

I then moved to the next sketch, one of the long sides of the box. I had to choose which planes I wanted the new sketch to align with. So I chose between the teeth to make sure it was aligned on the correct plane.

![new sketch side](new-sketch-side.jpg)

I added the teeth in the appropriate places and extruded the sketch. I had to make the distance “-ply” so that the extrude went the correct direction.

![negative ply function](negative-ply.jpg)

Then I mirrored that side, ensuring I chose the correct plane to mirror.

![mirror side fusion](mirror-side.jpg)

I continued to the other two sides. The box was complete!

![colorful box on fusion](colorful-box.jpg)

Then, thanks for Burak, I found out about a dxf add-on for Fusion that would allow me to pull the planes into dxf files for laser cutting and I could pre-add the kerf. I tried cutting the box with cardboard, and with plywood, but the kerf function in the dxf add-on did not help with the fit of the teeth. The teeth were slightly loose. I measure the width of the tooth and the negative, to get an idea of what was wrong!

![negative dimension of finger joint](negative-joint.jpg)
![positive dimension of finger joint](positive-joint.jpg)

I knew I needed to add a new parameter for kerf. I made one at 0.2mm. I spoke to Yu-Han who showed me a cute trick. It was confusing to understand how to make the teeth bigger and the negative smaller. Yu-Han showed me I could select the faces of the teeth, use the “Press Pull” function to add my kerf parameter to these sides.

![press pull offset function](presspull-offset.jpg)

So! I did this to my base plane sketch, I did not change my sides. I used the laser cutter again, did a square test to see if my kerf was the same for the fresh new piece of 3mm plywood, and it was. I cut my piece and HOLY MOLY GUACAMOLE it worked!! Check out that 90 degree, no support needed:

![snap fit strong](snap-fit-strong.jpg)

I am not sure I have had such a satisfying moment as snapping those teeth together with such precision! Am I addicted to precision?! To success?? To digital fabrication? What is this sensation!

![gabriela holding boxes](gabs-boxes.jpg)

_Square Test with Lukas_

On our snowy Saturday, Lukas and I worked together to measure kerf on campus. I have done this with every test, but I thought I’d add some photos to show the process.

Big snow day:

![snowy lukas](snowy-lukas.jpg)

Checking the laser position:

![laser position](laser-position.jpg)

Pre-kerf:

![pre ferf measurement](pre-kerf.jpg)

Post-kerf:

![post kerf](post-kerf.jpg)