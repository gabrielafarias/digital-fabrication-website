+++
title = "Project Management"
+++

This week we continue to improve our websites and learn media optimization. Our tasks are to:

- Create a page for the project management assignment on your website.
- Describe how you created your website.
- Describe how you use Git and GitLab to manage files - in your repository.
- Talk about how you capture and optimize your images and videos (optional).

## Step one Towards Git Repository

I began my website building journey (a completely uphill adventure) by downloading VSCode and building a basic HTML structure in a simple framework. I created a local repository on my desktop where this HTML file lives.

My Git Repository is where I am officially documenting my process in this course. It can be found [here.](https://gitlab.com/gabrielafarias/digital-fabrication-websitetarget=_blank)

![Screenshot of Git repository](git-repository-screenshot.jpg)

## How to Generate SSH Key

In short: Git is the version control system and GitLab is the Git management software that is home to my Git repository. In order to make the two communicate, I used SSH keys to authenticate my remote server. This is public key cryptography in ACTION, baby. A public key is published and a private key is kept secret. This is what the SSH key does when connecting my public repository to my private computer...

Using the sshkey-gen command I generated an SSH key on my local terminal. It made a long code and a cute little picture. In my user settings I added the SSH key to the box as seen below. I put my key in that box and said "add key" and bada bing bada boom CONNECTION.

![ssh key screenshot](ssh-key-locatsh.jpg)

Then I cloned the GitLab repository on my terminal. Below is a screenshot of Hiski helping me complete my SSH key process using my local terminal:

![Screenshot ssh completion](ssh-complete.jpg)

## Terminal Commands

I never worked with my computer's terminal until this course.I use Git commands in the terminal generally when I am getting ready to update my online repository. 

I start by opening my terminal and opening my local repository using:

    cd /Users/gfarias11/Desktop/Digital_Fabrication_2023/digital-fabrication-website

From there, I usually work with git or hugo commands. Generally with Hugo, I use it to update my local server by typing: hugo serve

My favorite Git commands in this order are:

    git status
    git add 
    git commit -m "message"
    git pull
    git push

I use Brew for installing new software. I use it by the command:

    brew install blank

This is the exact way I have downloaded Hugo, VSCode, ImageMagick, NDCU, SolveSpace, and OpenCAD.

## How to Add a Video

I had one GIF to add to my Final Project section of Kieron wearing a earplug Plug N Play earring.

In order to do so, I added the video source to the index.md under the final project folder as:

![Video HTML code](image1-adding-video.png)

This was not enough. I added some HTML that I sourced from the link below to my shortcodes video.html file:
https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video

You can see the html below:

![Screenshot of shortcodes video html](image2-adding-video.png)

The video worked, but the images didn’t! UGH.

![Screenshot website and vscode wrong](image3-adding-video.jpg)

After 40 minutes of troubleshooting, I realized I had simply not closed the video brackets. And voila, the photos are back. The code I was missing is displayed above with the error VSCode was giving me, and then below is how I fixed the HTML:

![Screenshot of video code in final project index](image4-adding-video.png)

My next problem was the only way to get the video to work and photos to work at the same time resulted in the tag “Kieron in earring: kieron_gif.mp4”. To be continued!

## How to Resize using ffmpeg

I also used ffmpeg to change the size of a video that was much bigger. The original video was 83MB. I used the following code to resize it:

    ffmpeg=7 test-for-ffmpea.mp4 -C:V libx264 -crf 23 -c:a aac -b:a 128k test-for-ffmpeg-resized.mov

Using this in my terminal resized the video to 49.5MB and changed it from an mp4 to a mov file. I tried the same code again and changed the crf to -35 and this took the video down to 13.4MB. The quality is not bad!

## How to Use ImageMagick

Below is a list of ImageMagick commands

    ImageMagick list formats:
        convert -list format
    JPG: compressed
    PNG: uncompressed
    convert PNG to JPG:
        convert input.pg output.jpg
    convert all PNGs to JPGS:
    mogrify -format jpg *.png
    convert SVG to PNG at 1000 DPI:
        convert -density 1000 -units PixelsPerInch input.svg output.png
    compress JPG to quality 50% width 1000:
        convert input. jpg -quality 50% -resize 1000 output. jpg
    compress all JPGs to quality 50 width 1000:
        mogrify -quality 50% -resize 1000 *. jpg

I was struggling with the *.jpg command so I asked ChatGPT to help me. The AI provided the following code, which worked:

![chatgpt screenshot](chatgpt-imagemagick.jpg)

Be sure to include the bin/bash bit, G!

OR JUST DO THIS TO CONVERT PNG TO JPG (FROM WITHIN THE FOLDER YOU WANT TO CHANGE)
    magick mogrify -format jpg *.png

I also resized an individual image using the same program, for example a screenshot of my earliest version of my website (Gabriela - you have come SO far!)

![chaz resized](imagemagick-commands-git.jpg)

## Using NDCU and Learning when to Commit

When I took the screenshot of my repository for this documentation page, I was wildly surprised at how much space my website was taking up! Kris recommended I download NDCU. I used brew to install it from my terminal. After downloading, I did not learn new information, but I did know how to use this software to check my files.

![ndcu screenshot](ndcu-check.jpg)

This let me know that even though my repository was 310MiB, none of my files included in my local repository were the problem. The artifacts are the issue, taking up 92% of my repository space! 

![my artifacts](artifacts.jpg)

Yikes! Apparently that will go away with time. But I learned to only commit when I am REALLY ready to commit!

I HAVE LEARNED SO MUCH AND I WILL KEEP LEARNING AND TELL THE WORLD ABOUT IT!
