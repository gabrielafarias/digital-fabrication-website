+++
title = "3D Printing and Scanning"
+++

## Design Test Rule

I decided to print the unsupported overhang test file from the Fab Academy website [here](http://academy.cba.mit.edu/classes/scanning_printing/index.html).

I downloaded the stl file, put it on my OneDrive and opened it on the laptop next to the Ultimaker printer. I opened the file in Cura and adjusted the settings for the test. I ran many of the settings on their defaults, but changed the infill pattern to Cubic Subdivision as per Hiski’s recommendation. Cura has 14 types of infill available for use. The following screenshot shows the best use for all of them:

![infill option](infill-options.jpg)

Cubic subdivision uses less material than some of the other infill options. I am curious to try using some additional infill patterns.

I made the printing temperature 210 degrees, the build plate temperature 60 degrees and changed the build plate adhesion type to “skirt” with no prime blob (I love that this is a technical term).

I also printed using 0.15mm profile (Normal) as per Hiski’s recommendation. This cut the printing time from 2 hours at a 0.1mm profile (which seems to be default) to one hour. I ran a preview to see how the Ultimaker 2+ Connect would print the test. Hiski was the S3 Baron for a while but now he has been sharing :)

{{<video src ="slicer-3d-test_enc.mp4">}}{{</video>}}

Looking at the internal structure of the overhang parts, I was slightly worried how it would print, but let the digital printing gods determine my first 3D print’s fate. Which wasn’t great! The first attempt on the Ultimaker 2+ Connect was unsuccessful.

{{<video src ="fail-test-print.mp4">}}{{</video>}}

Solomon let me know that machine was consistently having some trouble, so we changed the filament spool. I obviously chose “violet transparent.” 

Attempt #2 with the violet worked!

{{<video src ="success-test.mp4">}}{{</video>}}

The build was as to be expected up until the build of the overhangs started. I did not have any previous knowledge of how the 3D printer would start the unsupported overhang, but it makes sense: it started to build a small level of material and then grow that slowly out. That is why the longer overhang has bits hanging.

###__Learnings/Questions__

The overhang was imperfect from the “level 4”. It was unfortunately so hard to get a good closeup with my iPhone:

![test overhang print](test-closeup.jpg)

I assume if the temperature were hotter, it would have even more loose bits on the overhang, but I wonder with an adjustment to speed how else it would have effected the overhang. Overall, it doesn’t seem so bad.

I assume the S3 would have printed differently than the 2+Connect. I also assume if I had printed the piece on its back it would have printed more successfully.

## 3D Printing

_The Model_

I started with printing the earplug design I made from the CAD week. I want to know if it works in the ear and how it feels, and if I can connect the hardware for the earring and make a mold for a silicone outer shell. The inside is empty and the hole that cuts across the middle also seems it will be a challenge for 3D printing.

The best way to see this is my documentation from the [CAD week assignment](https://gabrielafarias.gitlab.io/digital-fabrication-website/assignments/computer-aided-design/).

Here is a video showing the empty spaces and the piece as a whole:
 
{{<video src ="3d-print-design.mp4">}}{{</video>}}

_3D Printing_

I started with the S3, printing with PLA, but let someone use it part way through my booking because I am a nice person, so overall there was differentiation in my prints because some were done on the 2+ Connect. I did my first print on the S3 with the “Fine 0.01” profile and attempted to use the second extruder in case it needed some sort of supports, but Cura did not seem to use them. I cannot remember what I used for the infill, but it was circular, I made two of the earplugs by multiplying the model by 1, and printed them on their “base” with a skirt. You can see the way Cura sliced it here, which shows the circular infill:

{{<video src ="cura-slice.mp4">}}{{</video>}}

The printing was happening without any issue. 
The thing is: I made them SO tiny!

![tiny earplugs](IMG_4700.jpg)

So! I had to print them again. I changed the size by increasing to 300% the size of the original. Now they were too big!

![300% on cura](IMG_4705.jpg)

I had already let someone else print on the S3 by now (an offering to receive good karma), so I printed my second one on the 2+Connect. I used cubic subdivision and from the beginning, noticed these were WAY too big, and the infill looked pretty bad, so I stopped the print.

![bad base of earplug](IMG_4709.jpg)

I changed the filed size to 200% and tried printing again. It failed.

![failed print](IMG_4714.jpg)

I stopped the print, but compared the base to the 300% size bases to see if I was on the right track to the appropriate size

![comparing failures](IMG_4716.jpg)

Okay! Try number 3. I printed again using the 2+ connect, and it seemed to be some problem printing the 2 earplugs at the same time. There was a challenge between the file in the original versus the copy. 

![fun failure on ultimaker](IMG_4724.jpg)

{{<video src ="FUN-ULTI-FAILURE.mp4">}}{{</video>}}

{{<video src ="failure-2.mp4">}}{{</video>}}

It seems the earplug did not adhere to the build plate properly from the beginning, I know it is not the ethos of DigiFab1, but I loved seeing the mistake versus the perfection. It really made me laugh, understanding the capabilities of the printer. I let the print continue because I knew I’d at least get one of the two right! And it was more successful. The one earplug was a good size.

I went back to the S3 finally – and decided to print one earplug at a time. Yu-han gave me advice to change the printing temperature and speed, so I adjusted those from 210 to 200 print temp and from 65 to 70 print speed. I understand I was changing printers again, but I did this change anyway. I also did it upside down as an experiment, and messed up because I used brim instead of skirt, so the top of the earplug was closed. Not a bad thing. I had also shrunk the size from 200% to 150%, which was finally my Goldilocks moment! 150% of the original drawing is the perfect size.

{{<video src ="final-plug.mp4">}}{{</video>}}

I printed one more time! This time and during the print it stopped because it said extruder 2 was out. But it wasn’t. The yellow filament had wrapped around the clear filament spool at the back, so I unspooled it and freed the transparent filament. However, it messed up my print. I took a photo while it was messed up to show the difference between the infill from the upside down vs right side up plugs.

![FINAL PRINT INFILL](IMG_4744.jpg)

In general, it seems the one printed upside down came out better! The right-side up one has a little spot like where the extruder left too much on top. I know it can be cleaned up. But the problem is there is a break in the print from the pause.

![difference in plugs](IMG_4745.jpg)

![diff in prints rightside up](IMG_4747.jpg)

SO! Phew, a lot of not great or perfect prints but a lot of attempts. In the end, I think these need the silicone top to see if they’d actually properly work. Here is a picture of all the gals in their glory, from first print to last print:

![all the prints together](IMG_4772.jpg)

## 3D Scanning Experiment

I brought in a shell I found at Cape Point in South Africa while hunting for kreef. It is called a Venus Ear!

![venus-front](venus-front.jpg)
![venus back](venus-back.jpg)

It was clear this would be a challenging this to 3D Scan. The ripples and folds cast shadows and would be difficult to capture, and the shiny backside like abalone would be difficult for the laser to register. I was determined to prove that nature is superior to technology, so I went on with it.

Solomon gave me a training on how to use our friend Leo, the Artec 3D Scanner. The machine is not challenging to use but is challenging to get the hang of distance from the object and how to assist Leo to understand an axis of the object. You start a new project, and each scan you do is added to that file. 

Your goal is to find the green area, that is what Leo is scanning. I started with the shell laying on a black surface on a table. 

![venus v leo](venus-vs-leo.jpg)

It was hard to find Venus in the image in the lighting. I needed to figure out a way to get all sides of the image. I made a stand on a chair in the wood shop because I could scan in the dark there. I could then walk around the object, which I clipped at a bottom edge. Solomon also showed a vanishing scanning spray to make the shell matte and potentially easier for Leo to read. The following shows how I scanned in the new configuration (this round with the light on).

{{<video src ="3d-scanning.mp4">}}{{</video>}}

It came out a lot more successful

![venus on leo](IMG_4718.jpg)

We took Leo to the computer to upload the project to the Artec software. You plug in Leo to the computer and connect to upload this way. We used the latest Scan Group to mesh, and this was so cool! Solomon helped show me the software a bit. We came up w an interesting model:

{{<video src ="artec-1.mp4">}}{{</video>}}

I did one more scan and uploaded it to try and get the sides and base of the shell a bit more clearly defined in the scan. This was successful.

{{<video src ="artec-2-edit.mp4">}}{{</video>}}

Haha I also need to learn how to navigate the axis better :P. I need to do more mesh simplification with Mesh Maker. I will learn that and then try printing the shell!

### Accomplishments and Interests

_This week I was able to:_
-	3d print test
-	3d printing
-	3d scanning and playing with mesh simplification

_What I Want to Do:_
-   Laser Cut my lamp Wednesday
-	Mesh the 3D Scan file! Next Tuesday or Wednesday!
-	Print the shell on Thursday
-	Fusion Generative design
-	Blender
-   play more with fusion
-   Book a flight home, Gabriela! NB!

_Notes:_
-   when using ffmpeg: 
    - crf as a higher number will make your file 
    - i was running the code incorrectly, use this:
    ffmpeg -i input-file.mp4 -c:v libx264 -crf 32  -c:a aac -b:a 128k output-file-diff-name.mp4
