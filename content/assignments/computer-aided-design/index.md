+++
title = "Computer-Aided Design"
+++

This week we learn Computer-aided design software. Neil went over about 400 options at rapid speed, as he does. The 3D design programs include:

1. **SolveSpace**: Free, open-source platform that Kris recommends. I will explore this option for sure.
2. **FreeCAD**: Free and open-source 3D computer graphics software toolset
3. **Fusion360**: 3D CAD, cloud-based software. Autodesk program, seems most relative to design work vs architecture and most community knows it.
4. **Blender**: Free and open-source 3D computer graphics software toolset. Start with the gemoetry nodes tutorial from Blender Guru.
5. **AutoCAD**: More appropriate for architects, same with Revit. Part of the Autodesk empire.
6. **SculptGL**: Not a CAD software, but a fun platform to play with sculpting and rendering tools. [Found here](https://stephaneginier.com/sculptgl/)

I have no experience with 2D or 3D drawing, I've lived quite a Luddite lifestyle til now! So, I started with Fusion 360.

I started with a tutorial on Youtube called “Fusion 360 Tutorial for Absolute Beginners (2020)” by [Product Design Online channel](https://www.youtube.com/watch?v=qvrHuaHhqHI)

**Keyboard shortcuts learned:**

    L = activates line command
    Tab locks angle into place
    Escape = exit line command
    E= Extrude
    P= project
    C= center circle tool
    D=dimension (this one came from my dude sweet Paul McWhorter who also loves to say [boom and loves ice coffee](https://www.youtube.com/watch?v=y5tp4QXciK4&list=PLGs0VKk2DiYx15SfBxO_VE6ELhpy0VnAw&index=2)

**Tools/Views:**

Top right corner shows view options can work with
“Construction” in line command is dotted as something to work with, not part of final design

**First adventure:**

- Drew a 35mm line, with a 5mm line at 70 degrees off the right side.
- Did same thing on the left side. Closed the box and made a shape.
- Used extrude command under solid > create
- MADE 3D object
- Added SVG file and highlighted the bits I wanted extruded to make the stamp.
- Created new component (handle)
- Chose “midplane” under “construct” and chose to end planes to find center of the stamp
- to create dovetail – use tool called “project” (shortcut - P) 
        - after activating project, click midplane as that’s what we want the new sketch to be orients view for easier working
        - project lets us select geometry and converts it into 2d sketch geometry on the active plane
        - choose lines or plane you want
        - purple lines show model based off current geometry
- making the handle
        - used c for circle after making diagonal line w construction line tool
        - have to make an offset plane to make top of the handle. 
        - Choose c again
        - Make a circle above on that offset plane
        - Choose “loft” under solid >> create
        - Round the edges on the handle
        - Use fillet command under “modify”
        - Choose your fillets!
        - Can export as stl? On this version of fusion? 
        - Can also export right away to Cura for 3d slicing!

I also used (https://www.youtube.com/watch?v=_SOZ7x-gyoE&t=2s) to learn how to change dimensions in my sketch after I messed up the first time.

## Making my earplug piece with Fusion360

- I found a model that already existed and used FreeCAD to find the measurements.

![freecad plug w measurements](freecad-plug-measurements.jpg)

- I based my measurements loosely off of that and measuring earplugs I had at home. At this point, I am playing around for the right size to hold the earplug tip.
- I started a new project in Fusion360 called “earplug project”
- I built half of the earplug very easily, using the L command to create lines and filling in the mm with measurements I had taken
- Then I located the “revolve” function under solid then create
- A box popped up, and I chose the profile I wanted to revolve and around which axis (axis Z). The operation was to “join”
- And boom!

![Revolve in Fusion](revolve-fusion.jpg)

I filleted the top and the bottom edges that I wanted filleted.

Okay I was looking good but I couldn’t extrude properly. I realized in a video I watched with Paul McWhorter, the biggest iced coffee fan I have ever encountered, that I didn’t make all my lines black and therefore long story short – I needed to start over.

So I did. I made sure each line was black. One line was green. I found on Autodesk I needed to fill the white dots to give the final line constraints – it went black.

Before:
![shape with green line](og-shape-fusion.jpg)

After:
![pesky dot on fusion](pesky-dot-lock-fusion.jpg)

Now!
Solid >> create>> revolve >> choose plane and choose axis you want to revolve around
Boom

![Revolve in fusion](revolve-fusion.jpg)

Fillet the edges
Make a hole thru the center – this will be a test run on the position of the hole

![Fillet in fusion](fillet-fusion.jpg)

Here is the final image:

![Final fusion image](final-fusion.jpg)

## SolveSpace Attempt

I followed the first tutorial recommended from Youtube:
https://www.youtube.com/watch?v=WlEHUJhgBuU

_Shortcuts:_

A list of shortcuts is here: <https://shortcutworld.com/SolveSpace/win/SolveSpace_Shortcuts>

        V=vertical (constrain points to vertical plane)
        O=origin (constraints to the origin)
        G= means “toggle construction” which I had to do to make solvespace less mad about my contours
        D= diameter (identify what you want it to be)

_My Journey_

Within the first 2 steps, I was lost. I kept following the commands the dude in the video did, but my screen would go red or give me a box explaining what the issue was. I am instantly frustrated because I don’t know what these things mean.

![view of solvespace](bad-constraint-1.jpg)

After looking very closely with my eyeballs I saw he didn’t actually line the open curve on the vertical axis, it was slightly off, that’s how he got his two fucking points he could grab to snap to vertical, and then the O worked fine. Sometimes you have to walk away and say how much you hate the computer to be able to come back and then beat the computer.

Okay so I made the half circle, constrained it to origin and vertical. Then I made a rectangle. 
By clicking bottom line of that rectangle and the bottom point of the curve, I was able to align the curve with the bottom line of the rectangle.

Solve space doesn’t like open contours it is consistently telling you so. By choosing a contour and pressing g, the red all caps letters telling you it hates open contours go away.

Then using D for diameter, put in that number and click use radius in the floating box to identify “use diameter” and make it the right diameter (15)

I made all of this work, but then got very hung up on constraints, degrees of freedom, and was unable to extrude the plane. I tried to figure this out for far too long and was unable to find the resources to help me as easily as I could with Fusion, as a total beginner. Most of the tutorials for Solvespace are much faster than the ones you can find for Fusion and harder to follow.

![failure to extrude in solvespace](cant-extrude.jpg)

I really do, with all my heart, want to be able to make Solvespace work. But due to the rapid pace of the course and the next deadline amongst my other 12 credits of deadlines, I have to throw in the towel. I want to dedicate my time to learning Fusion for now. This course has taught me how to do Youtube tutorials, and I miss Paul McWhoter, my iced coffee loving Southern gentleman, my muse. When my time comes to learn a free software, I will utilize Youtube as I now have really learned how.

![paul sippin coffee](PAUL-MCW-1.jpg)

![paul using his hands](paul-mcw-2.jpg)

## JUST KIDDING: I TRIED SOLVESPACE AGAIN

Okay, I see your 3 and I raise you a completed solvespace project!
It is a Friday evening but I can’t stop. DigiFab has turned me into a monster!!!

I am trying a new tutorial with a man with a sweet accent and a face and he moves slower:
https://www.youtube.com/watch?v=ckkGfngoSaE

Here is the link so SolveSpace Reference Manual:
https://solvespace.com/ref.pl

He sketched a rough sketch of the first face, and then went in using the H and V keys to make sure the lines were horizontal and vertical.

![h and v commands in solvespace](H-and-V.jpg)

Then we made the constraints according to dimensions.

![constraints in solvespace](distance-constraints.jpg)

For the longest distance, you don’t pick the two points at the bottom line, because those will just give you shortest distance between those two points. You should choose the point and the line to find that distance.

You go around and add all the distances, and the degrees of freedom go down, because with every constraint you remove a degree of freedom.

By clicking any point and then clicking the origin point, I got to 0 degrees of freedom YAY! I DID IT!!!

Oh my god okay then I was able to extrude but couldn’t figure out how to view.
Newsflash: YOU HOLD DOWN CENTER MOUSE BUTTON TO PAN!
To pan the view you right drag the mouse.
wow

okay I extruded and I set the distance, nbd, im a pro

Now! I chose 2 lines and a point to specify the plane I am trying to work on next when I click on the “new workspace” button.

![how to get a new workspace](new-workspace.jpg)

Then I made a rectangle on that plane, constrained it, extruded it, but did the “difference” and made it the properly distance.

![extrude difference solvespace](extrude-difference.jpg)

Then! He showed us how to go back in our history to change any mistakes. I will say that this feature is easier than Fusion, now that I understand it. We went back and changed a distance that previously was incorrect. And then back to normal

![history in solvespace](history-to-change.jpg)

Then we add the circles. I made them two different sizes intentionally to see how to use the “constrain equal length” button and then set them both vertical to align them.

![aligned circles](aligned-circles.jpg)

Then I extruded them, used the different and constrainted to the plane so they would be flush.

![circle magic in solvespace](extrude-circle.jpg)

And tahdah – I did it.

Next I will try Kris the stool you made! Yay!
