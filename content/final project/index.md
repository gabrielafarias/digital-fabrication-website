+++
title = "Final Project Ideas"
+++

For my final project, I have 4 concepts in mind.

For Digital Fabrication 1, I'd like to create a mold for earplug earrings I make. Below is a photo of the first version of the earrings:

{{<video src="kieron_gif.mp4">}}

People love these simple things! However, I would like to make a mold for replaceable earrings that fit on a fixture attached to a chain earring, as the earplugs I am currently using start to get a bit gross after time. If the fixture isn't possible, I'd like to make a silicone mold so the earplug can be easy to clean.

I've done some research so far. Below are 3 types of earplugs I own.

![3 types of earplugs](earplugs_all.jpg)

I examined the pairs attached to plastic cord. The yellow earplugs are graded for airplane engine mechanics and to remove the plug would have ruined them. The orange pair I was willing to cut open and see how deep the cord was inserted. It seems the plastic cord went about 60% of the way into the mold.

![Earplug cut open](earplugs_cut.jpg)

I am considering 2 designs, one similar to the original design that uses a stiffer plug like that of the airplane mechanic plugs, and incorporates the straight attachment piece which helps for insertion. The second design would be more organic in shape and have the ability to replace the plug. See my inspiration for that below.

![Hexarmor screenshot](hexarmor_screenshot.png)

The following are my drawings for my Plug 'N' Play earrings.

![Drawing of earplug idea 1](idea_1.jpg)
![Drawing of earplug idea](idea_2.jpg)

For my FINAL final project, I am considering 3 ideas.

1. A hydroponics system to use at The Test Site
2. A mycellium cultivator
3. A motorcycle direction assist tool

I ride motorcycle and find phone mounts quite finnicky and distracting. I have no desire to look at my phone when I'm on my bike. So, a simple gadget that can let me know the important stuff is all I want: when to turn, which direction, and how soon. See my simple drawing below:

![Drawing of motorcycle idea](idea_3.jpg)
