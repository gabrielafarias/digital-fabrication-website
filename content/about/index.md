+++
title = "About me"
+++

My name is Gabriela Farias and I am a Brazilian-American fabricator and activist in my first year of the Creative Sustainability Master's in Design at Aalto University in Helsinki, Finland. I have worked on many diverse design projects, from large-scale sculptures for Burning Man as a part of their Metal Shop staff, to hyper-realistic trees and mushrooms for museums and nature centers. However, my projects as an activist have been my most impactful work.

I am delighted to be a part of the Digital Fabrication program to expand my physical design knowledge and understand what is available to and how to use these tools that are new-to-me to become a better designer.

![Picture of me in t-rex](sue_head.jpg)

## Some of my work

![R4RR motorcycles in St Paul](R4RR-234.jpg)

The Ride for Reproductive Rights was started by my self and Mallory Helmerick in 2019. In 2022, the US Supreme Court overturned Roe V Wade 2 weeks before the Ride. Our turnout and funds raised doubled from the first year: 80 motorcycles, over 200 attendees, over 50 donated prizes and $9000 raised. We look forward to the 2023 edition! For more images, you can see [here.](https://hmscreative.pixieset.com/r4rr/target=_blank)
